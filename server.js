const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const mongoose = require("mongoose");
const dbURI = `mongodb+srv://server:password12345@cluster0.w93q1.mongodb.net/job-portal-backend?retryWrites=true&w=majority`;
const app = express();
const PORT = 8000;

app.set("view engine", "pug");
app.set("views", "views");

const sequelize = require("./util/database");
const adminRoutes = require("./routes/admin");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", adminRoutes);

app.get("/about-us", (req, res, next) => {
  res.render("about-us", {
    pageTitle: "About Us",
    path: "about-us",
  });
});
(async () => {
  try {
    await mongoose.connect(dbURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    await sequelize.sync();

    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  } catch (err) {
    console.log("error: " + err);
  }
})();
